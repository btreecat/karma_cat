'''
Created on May 28, 2011
This is the basic Admin plugin
This plugin must always be loaded
to perform any administrative features.
@author: stanner
'''
from Plugin import Plugin
import sys

class Admin(Plugin):
    '''
    classdocs
    '''
    def __init__(self, acl, server):
        #Plugin.__init__(self, acl, server)
        super(Admin, self).__init__(acl, server)

        self.ownerCommands = {':,add_admin':self.__addAdmin,
                              ':,del_admin':self.__delAdmin,
                              ':,die':self.__die,
                              ':,join':self.__joinChan,
                              ':,part':self.__partChan}

        self.adminCommands = {':,mute':self.__muteServer,
                              ':,unmute':self.__unmuteServer,
                              ':,ignore':self.__addIgnore,
                              ':,unignore':self.__delIgnore}

        self.anonCommands = {':,get_admin':self.__getAdmin,
                             ':,help':self.help,
                             ':,get_ignore':self.__getIgnore,
                             ':,get_chans':self.__getChans }

    def read(self, message):
        '''You are owner'''
        if self.ACL.getPermLvl(message) == 0 :
            try:
                self.ownerCommands[message.message[0]](message)
            except KeyError:
                pass

        '''You are an admin'''
        if self.ACL.getPermLvl(message) <= 1 :
            try:
                self.adminCommands[message.message[0]](message)
            except KeyError:
                pass

        '''You are neither'''
        try:
            self.anonCommands[message.message[0]](message)
        except KeyError:
            pass

    def __getChans(self, message):
        '''PM user a list of the chans the bot is in'''
        self.SERVER.sendPRIVMessage(message.nick + ' :' +
                                    str(self.SERVER.chans))

    def __joinChan(self, message):
        '''Have the bot join a chan'''
        if len(message.message) > 1:
            self.SERVER.joinChannel(message.message[1])

    def __partChan(self, messsage):
        '''Have the bot leave a chan'''
        if len(messsage.message) > 1:
            self.SERVER.partChannel(messsage.message[1])

    def __getAdmin(self, message):
        '''PM user a list of admins'''
        self.SERVER.sendPRIVMessage(message.nick + " :admins: " +
                                    str(self.ACL.getAdmins()))

    def __getIgnore(self, message):
        '''PM user a list of ignored user(s)'''
        self.SERVER.sendPRIVMessage(message.nick + ' :ignored: ' +
                                    str(self.ACL.getIgnore()))


    def __muteServer(self, message):
        '''Tell the bot to send no replies to the server'''
        self.SERVER.sendPRIVMessage(message.nick + ' :' +
                                    'Cutting communications...')
        self.SERVER.mute()

    def __unmuteServer(self, message):
        '''Tell the bot to allow replies to the server'''
        self.SERVER.unMute()
        self.SERVER.sendPRIVMessage(message.nick + ' :' +
                                    'Establishing link...')

    def __addAdmin(self, message):
        '''Add an admin to the admin list'''
        if len(message.message) > 1:
            self.ACL.addAdmin(message.message[1])
            self.SERVER.sendPRIVMessage(message.nick + " :" +
                                        message.message[1] +
                                        " added to admins")

    def __delAdmin(self, message):
        '''Remove an admin from the admin list'''
        if len(message.message) > 1:
            self.ACL.delAdmin(message.message[1])
            self.SERVER.sendPRIVMessage(message.nick + " :" +
                                        message.message[1] +
                                        " removed from admins")

    def __die(self, message):
        '''Tell the bot to die'''
        print('Master commanded me to quit')
        self.SERVER.unMute()
        self.SERVER.sendPRIVMessage(message.nick + " :" + " quitting...")
        self.SERVER.sendMessage('QUIT')
        sys.exit(3)

    def __addIgnore(self, message):
        '''Add a user to the ignore list'''
        if len(message.message) > 1:
            self.ACL.addIgnore(self.SERVER.getWhois(message.message[1]))
            self.SERVER.sendPRIVMessage(message.message[1] + ' :' +
                                        'You have been added to the ignore list. Probably because you are annoying...')

    def __delIgnore(self, message):
        '''Remove a user from the ignore list'''
        if len(message.message) > 1:
            self.ACL.delIgnore(message.message[1])
            self.SERVER.sendPRIVMessage(message.message[1] + ' :' +
                                        'You have been removed from the ignore list, for now...')

    def mainHelp(self, message):
        helpstr = ['Meow Meow I am Karma Kat! ',
                  '  Owner     = ' + str(self.ACL.getOwner()),
                  '  Admins    = ' + str(self.ACL.getAdmins()),
                  '  Ignored   = ' + str(self.ACL.getIgnore()),
                  '  Plugins   = admin, karma, url, cccb',
                  '  More help - ,help <plugin name> to get the help page for a plugin']
        for hlp in helpstr:
            self.SERVER.sendPRIVMessage(message.nick + ' :' + hlp)

    def help(self, message):

        if len(message.message) == 1:
            self.mainHelp(message)
        else:
            if message.message[1] == 'admin':
                cmds = list(self.ownerCommands.keys())
                cmds.extend(list(self.adminCommands.keys()))
                cmds.extend(list(self.anonCommands.keys()))

                helpstr = ['Admin Help Commands: ' + str(cmds).replace(':', ''),
                           '  ,add_admin  - ' + self.__addAdmin.__doc__,
                           '  ,del_admin  - ' + self.__delAdmin.__doc__,
                           '  ,die        - ' + self.__die.__doc__,
                           '  ,get_admin  - ' + self.__getAdmin.__doc__,
                           '  ,mute       - ' + self.__muteServer.__doc__,
                           '  ,unmute     - ' + self.__unmuteServer.__doc__,
                           '  ,join       - ' + self.__joinChan.__doc__,
                           '  ,part       - ' + self.__partChan.__doc__,
                           '  ,ignore     - ' + self.__addIgnore.__doc__,
                           '  ,unignore   - ' + self.__delIgnore.__doc__,
                           '  ,get_ignore - ' + self.__getIgnore.__doc__,
                           '  ,get_chans  - ' + self.__getChans.__doc__ ]

                for hlp in helpstr:
                    self.SERVER.sendPRIVMessage(message.nick + ' :' + hlp)
