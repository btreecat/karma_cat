


class Message:

    ''' This class is used to hold a message for processing and passing around. '''


    def __init__(self, line):

        self.line = line
        self.nick = ""
        self.uname = ""
        self.host = ""
        self.type = ""
        self.chan = ""
        self.message = []

        self.__parseLine__(line)



    def __parseLine__(self, line):

        '''Message Variables to hold message awesome sauce
        Format:
        :nick!username@host PRIVMSG channel/bot-nick :Message 
        Example Input:
        [':btreecat!stanner@luug3.ece.vt.edu', 'PRIVMSG', '#btreecat', ':meow', 'meow']
        '''

        senderInfo = line[0].partition('!')
        self.nick = senderInfo[0].lstrip(':')
        senderInfo = senderInfo[2].partition('@')
        self.uname = senderInfo[0]
        self.host = senderInfo[2]
        self.type = line[1]
        self.chan = line[2]
        for word in line[3:]:
            self.message.append(word)


    def __str__(self):
        return str(self.message)
