'''
Created on Jun 9, 2011
This plugin is for shortening URLs

Since I use wee-chat, long urls require multiple
copy and paste actions to get the url into the
browser such that I can actually go the the 
right link. This plugin will automatically detect
any URL's typed and will call a remote url
shortening service and return a string
with the shortened URL which will be
sent to the chan

Portion of code (the 

@author: stanner
'''
from Plugin import Plugin
import urllib.request
from test.test_iterlen import len
from collections import deque

class URL(Plugin):
    '''
    classdocs
    '''

    def __init__(self, acl, server):
        #Plugin.__init__(self, acl, server)
        super(URL, self).__init__(acl, server)

        self.urls = deque([])

        self.knownCommands = {':,sl':self.__shortLink,
                              ':,psl':self.__privateShortLink,
                              ':,help':self.help }


    def read(self, message):
        '''Read the message from the server and proccess it'''
        try:
            self.knownCommands[message.message[0]](message)
        except:
            pass

        for word in message.message:
            word = word.lstrip(':').lower()
            word = word.strip()
            if word.startswith('http://') or word.startswith('https://') or word.startswith('www.'):
                ''' We can assume we are looking at a url hopefully'''
                if len(word) > 10:
                    '''Only shorten url if it will actualy be shorter'''
                    shortURL = self.__shorten(word)
                    if shortURL != None:
                        self.__addURL(shortURL)
                    else:
                        pass
                else:
                    pass
            else:
                pass
    def __shorten(self, url):

        if url.startswith('www.'):
            url = 'http://' + url
            #print( url )

        is_gd = 'http://is.gd/create.php?format=simple&url='
        try:
            reply = urllib.request.urlopen(is_gd + url).read()
            return str(reply)[2:-1]
        except urllib.error.HTTPError:
            return "URL not shortened"


    def __addURL(self, url):
        if len(self.urls) < 10:
            self.urls.append(url)
        else:
            self.urls.popleft()
            self.urls.append(url)

    def __getLastURL(self):
        if len(self.urls) > 0:
            return self.urls[-1]
        else:
            return "No URLs shortened"

    def __getURL(self, number):
        #print (numer)
        if len(self.urls) >= number:
            return self.urls[(number * (-1))]
        else:
            return "number out of bounds"

    def help(self, message):
        if len(message.message) > 1 and message.message[1] == 'url':
            cmds = list(self.knownCommands.keys())
            helpstr = ['URL Help Commands: ' + str(cmds).replace(':', ''),
                       '  ,sl  - ' + self.__shortLink.__doc__,
                       '  ,psl - ' + self.__privateShortLink.__doc__,
                       '  Note: {1...10} is an optional argument to get older links' ]

            for hlp in helpstr:
                self.SERVER.sendPRIVMessage(message.nick + ' :' + hlp)

    def __shortLink(self, message):
        '''Sends a shortened link to the chan.'''
        if len(message.message) == 1:
            self.SERVER.sendPRIVMessage(message.chan + ' :' + self.__getLastURL())

        else :
            if message.message[1].isdigit():
                idx = int(message.message[1])
                self.SERVER.sendPRIVMessage(message.chan + ' :' + self.__getURL(idx))

    def __privateShortLink(self, message):
        '''PM a shortened link to user'''
        if message.message[0] == ':,psl' and len(message.message) == 1:
            self.SERVER.sendPRIVMessage(message.nick + ' :' + self.__getLastURL())
        else:
            if message.message[1].isdigit():
                idx = int(message.message[1])
                self.SERVER.sendPRIVMessage(message.nick + ' :' + self.__getURL(idx))

