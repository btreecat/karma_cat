'''
Created on May 28, 2011

@author: btreecat
'''

class Plugin():
    '''
    This is the Plugin class. If you want to add
    a new feature to the bot, all you have to do 
    is extend the plugin class. 
    Then make sure your plug in is in the config file
    so an instance can be created of it and it 
    will be registered with the speaker.
    This way, every enabled plugin will be sent the message
    and the plugins can parse it and decided if they need to handle it
    or not.
    '''
    def __init__(self, acl, server):
        self.ACL = acl
        self.SERVER = server


    def register(self, speaker):
        speaker.register(self)

    def unregister(self, speaker):
        speaker.unregister(self)

    def read(self, message):
        pass

    def help(self, message):
        helpstr = []
        for hlp in helpstr:
            self.SERVER.sendPRIVMessage(message.nick + ' :' + hlp)

    #def help(self, command):
    #    helpstr = ""
    #    return helpstr
