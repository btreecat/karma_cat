'''
Created on May 28, 2011

@author: stanner
'''
import sqlite3

class ACL:
    '''
    classdocs
    This is the Acess Control List class.
    This class can be used by a plugin to check the permissions 
    before deciding to run the command
    '''

    def __init__(self, owner):
        '''
        Constructor
        '''
        self.OWNER = owner

        self.ADMINS = []

        self.IGNORE = []

        self.conn = sqlite3.connect('acl.db') #@UndefinedVariable

        self.curs = self.conn.cursor()

        self.__mkATable()
        self.__mkITable()




    def getAdmins(self):
        self.curs.execute('SELECT nick from admin')

        return [x[0] for x in self.curs.fetchall()]

    def getOwner(self):
        return self.OWNER

    def addAdmin(self, nick):
        if nick in self.getAdmins():
            pass
        else:
            self.curs.execute('INSERT INTO admin values (?)', [nick])
            self.conn.commit()
        #return self.ADMINS.append(admin)

    def delAdmin(self, nick):
        if nick in self.getAdmins():
            self.curs.execute('DELETE FROM admin WHERE nick = ?', [nick])
            self.conn.commit()
        else:
            pass
        #return self.ADMINS.remove(admin)

    def getPermLvl(self, message):

        self.ADMINS = self.getAdmins()
        self.curs.execute('SELECT nick, uname, host from ignore')

        self.IGNORE = self.curs.fetchall()
        #print(message.uname)
        #print("The ignore results: " + str([x[1] for x in self.IGNORE]))
        if message.nick == self.OWNER:
            return 0
        elif message.nick in self.ADMINS:
            return 1
        elif message.nick in [x[0] for x in self.IGNORE]:
            return 9
        elif message.uname in [x[1] for x in self.IGNORE]:
            return 9
        #elif message.host in self.IGNORE:
            #return 9
        else:
            return 2

    def addIgnore(self, user):
        self.curs.execute('INSERT INTO ignore values (?, ?, ?)', user)
        self.conn.commit()
#        return self.IGNORE.append(user)

    def delIgnore(self, nick):
        self.curs.execute('DELETE FROM ignore WHERE nick = ?', [nick])
        self.conn.commit()
        #return self.IGNORE.remove(user)

    def getIgnore(self):
        self.curs.execute('SELECT nick, uname, host FROM ignore')
        return self.curs.fetchall()
        #return self.IGNORE

    def __mkITable(self):
        '''This method is used to make a new table'''
        self.curs.execute(' CREATE TABLE IF NOT EXISTS ignore (nick TEXT PRIMARY KEY, uname TEXT, host TEXT)')
        self.conn.commit()

    def __mkATable(self):
        '''This method is used to make a new table'''
        self.curs.execute(' CREATE TABLE IF NOT EXISTS admin (nick TEXT PRIMARY KEY)')
        self.conn.commit()
