#import Command
from Message import Message

class Parser():
    '''
    The Parser class will parse each line from the server and determin how to 
    handle it. Possible ways to handle:
        Identify kinds of commands (raw, voting, games?)
        Run admin functions prifixed with botname(join, part, quit, etc. )
    '''

    def __init__(self, server):
        '''
        This init method sets up the Parser
        '''
        self.listeners = []

        self.server = server



    def parseLine(self, line):
        '''This method takes a line from the server as input 
        and will parse it'''

        if len(line) >= 4:
            '''We know the message is PROBABLY parseable'''
            message = Message(line)

            for l in self.listeners:
                l.read(message)


    def __sortMessage(self):
        '''This method is used to look at the first word 
        and see what kind of command it is'''
        if (self.message.message[0] == 'raw'):
            pass

    def parseEngine(self):
        '''This method is the driver behind the parser. It will 
        loop through fetching new lines from the server and processing
        the messages
        '''
        while True:

            line = self.server.getLine()
            self.parseLine(line)
        pass

    def register(self, listener):
        self.listeners.append(listener)
        return self.listeners

    def unregister(self, listener):
        self.listeners.remove(listener)
        return self.listeners


