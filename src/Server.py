import socket

class Server:

    def __init__(self, configDic):
        #This is the IRC server to connect to
        self.HOST = configDic['HOST']
        #Port on the server to connect to Usually 6667 or 7000 for SSL
        self.PORT = configDic['PORT']
        #Nick name for the bot
        self.NICK = configDic['NICK']
        #Idenity name
        self.IDENT = configDic['IDENT']
        #Real Name
        self.REALNAME = configDic['REALNAME']
        #Inital channels to start in
        self.CHANNELINIT = configDic['CHANNELINIT']
        #Used to store messages from the server
        self.readBuffer = ''
        #First we create the socket
        self.aSocket = socket.socket()
        #Should the server send replies?
        self.muted = False
        #Verbose
        self.verb = True
        #Logging
        self.log = False
        #Channels
        self.chans = []

    def connectToServer(self):
        '''Now we need to connect to the server'''

        self.aSocket.connect((self.HOST, self.PORT))
        self.sendMessage('NICK ' + self.NICK)
        self.sendMessage('USER ' + self.IDENT + ' ' +
                         self.HOST + ' bla :' + self.REALNAME)

        self.__chanAutoJoin()

    def joinChannel(self, chan):
        '''
        Use this to join to a channel
        '''
        self.sendMessage('JOIN ' + chan)
        self.chans.append(chan)

    def partChannel(self, chan):
        '''This method will let you part from a chan'''
        self.sendMessage('PART ' + chan)
        self.chans.remove(chan)


    def sendMessage(self, someMessage):
        '''This method will let other methods send messages to the server'''
        if self.muted == False:
                self.aSocket.send(bytes(someMessage + '\r\n', 'UTF-8'))

        if self.verb == True:
            print("-> " + someMessage)

        if self.log == True:
            '''Log the message to the file'''
            pass

    def sendPRIVMessage(self, someMessage):
        '''Short cut since most responses are PRIVMSG'''
        self.sendMessage('PRIVMSG ' + someMessage)

    def getLine (self):
        '''
        This method is to abstract the getting of the line
        '''
        line = None

        try:
            incomming = self.aSocket.recv(1024)
            self.readBuffer = self.readBuffer + bytes.decode(incomming)
            tempIn = str.split(self.readBuffer, "\n")
            self.readBuffer = tempIn.pop()

        except UnicodeDecodeError:
            print("There was an error decoding some Unicode from the server...")
            return self.getLine()

        for line in tempIn:
                line = str.rstrip(line)
                line = str.split(line)

        '''Pi(o)ng Detection'''
        if (line != None and line[0] == "PING"):
            self.__sendPong(line[1])

        print("<- " + str(line))
        return line

    def __sendPong (self, reply):
        '''This method sends a PONG reply 
        to the server with the supplied reply
        '''
        self.sendMessage("PONG " + reply)

    def __chanAutoJoin (self):
        '''This method is launched after connecting to the server and
            waits for your server to confirm the bot is identified on
            the server then calls join
            '''
        line = self.getLine()
        while line != [":" + self.NICK, 'MODE', self.NICK, ':+i']:
            line = self.getLine()

        self.joinChannel(self.CHANNELINIT)

    ''' This pair of methods is used to control whether or not the 
        will send its replies'''
    def mute(self):
        self.muted = True

    def unMute(self):
        self.muted = False

    def verbose(self):
        self.verb = True

    def quiet(self):
        self.verb = False

    def logOn(self):
        self.log = True

    def logOff(self):
        self.log = False


    def getUsers(self, chan):
        self.sendMessage('NAMES ' + chan)
        nicks = self.getLine()
        nicks = [nick.lstrip('+@:') for nick in nicks[5:]]
        return nicks

    def getWhois(self, nick):
        self.sendMessage('WHOIS ' + nick)
        info = self.getLine()
        return [info[3], info[4], info[5]]

