'''
Created on May 28, 2011
This will be the voting system plugin
@author: stanner
'''
from Plugin import Plugin
import sqlite3
import datetime

class Karma(Plugin):
    '''
    classdocs
    This is the Karma plugin
    It will handle the main parts of the karma system
    '''
    def __init__(self, acl, server):
        #Plugin.__init__(self, acl, server)
        super(Karma, self).__init__(acl, server)

        self.conn = sqlite3.connect('karma.db') #@UndefinedVariable

        self.curs = self.conn.cursor()

        self.__mkKTable()

        self.__mkWTable()

        self.ownerCommands = {':,nuke_karma':self.__nukeKarma,
                              ':,nuke_words':self.__nukeWords}

        self.adminCommands = {':,reset':self.__resetUser,
                              ':,del_word':self.__delWord}

        self.anonCommands = {':++':self.__upVote,
                             ':<3':self.__upVote,
                             ':--':self.__downVote,
                             ':</3':self.__downVote,
                             ':,karma':self.__getKarma,
                             ':,get_words':self.__getWords,
                             ':,help':self.help,
                             ':,get_users':self.__getUsers }

    def read(self, message):
        '''You are owner'''
        if self.ACL.getPermLvl(message) == 0 :
            try:
                self.ownerCommands[message.message[0]](message)
            except KeyError:
                pass

        '''You are admin'''
        if self.ACL.getPermLvl(message) <= 1 :
            try:
                self.adminCommands[message.message[0]](message)
            except KeyError:
                pass

        '''You are neither'''
        try:
            self.anonCommands[message.message[0]](message)
        except KeyError:
            pass

    def __getUsers(self, message):
        '''PM user a list of people who have voted or have karma'''
        self.curs.execute('SELECT uname from vote')
        names = [x[0] for x in self.curs.fetchall()]
        self.SERVER.sendPRIVMessage(message.nick + " :" +
                                    "Known users: " + str(names))

    def __nukeKarma(self, message):
        '''Nuke entire karma db Then create a new table'''
        self.curs.execute('DROP TABLE IF EXISTS vote')
        self.__mkKTable()

    def __nukeWords(self, message):
        '''Nuke entire word db Then create a new table'''
        self.curs.execute('DROP TABLE IF EXISTS vote2')
        self.__mkWTable()

    def __getWords(self, message):
        '''PM user a list of words with karma'''
        self.curs.execute('SELECT word FROM vote2')
        words = [x[0] for x in self.curs.fetchall()]
        self.SERVER.sendPRIVMessage(message.nick + " :" +
                                    "Known words: " + str(words))

    def __getKarma(self, message):
        '''Get the karma score for the user or word'''
        '''This method gets the karma score for a word or a user'''
        if len(message.message) > 1:
            karma = ['']
            self.curs.execute('SELECT * FROM vote WHERE uname = ?',
                              [message.message[1]])
            if len(self.curs.fetchall()) == 1 :
                karma = self.__totalKKarma(message.message[1])
            else:
                pass

            self.curs.execute('SELECT * FROM vote2 WHERE word = ?',
                          [message.message[1]])
            if len(self.curs.fetchall()) == 1 :
                karma = self.__totalWKarma(message.message[1])
            else:
                pass

            if message.chan == self.SERVER.NICK:
                message.chan = message.nick

            if len(karma) == 4:
                self.SERVER.sendPRIVMessage(message.chan + " :"
                                            + message.message[1] +
                                            " has a karma of: " + str(karma[0]) +
                                            " (U:" + str(karma[1]) + " D:" +
                                            str(karma[2]) + " V:" +
                                            str(karma[3]) + ")")
            elif len(karma) == 3:
                self.SERVER.sendPRIVMessage(message.chan + " :" +
                                            message.message[1] +
                                            " has a karma of: " + str(karma[0]) +
                                            " (U:" + str(karma[1]) + " D:" +
                                            str(karma[2]) + ")")
            else:
                pass

    def __upVote(self, message):
        '''Increase the karma score for a word or user by one'''
        if len(message.message) > 1:
            self.curs.execute('SELECT uname FROM vote WHERE uname = ?',
                              [message.nick])
            if len(self.curs.fetchall()) == 1 :
                if self.__nickCheck(message) :
                    if self.__isAllowed(message):
                        self.__pOne(message.message[1], message.nick)
                    else:
                        pass
                elif self.__wordCheck(message):
                    if self.__isAllowed(message):
                        self.__wUp(message.message[1], message.nick)
                    else:
                        pass
            else:
                '''create a new user, and return the results'''
                self.__newUser(message.nick)
                self.__upVote(message)

    def __downVote(self, message):
        '''Decease a karma score for a word or a user by one'''
        if len(message.message) > 1:
            self.curs.execute('SELECT * FROM vote WHERE uname = ?',
                              [message.nick])
            if len(self.curs.fetchall()) == 1 :
                if self.__nickCheck(message):
                    if self.__isAllowed(message):
                        self.__mOne(message.message[1], message.nick)
                    else:
                        pass
                elif self.__wordCheck(message):
                    if self.__isAllowed(message):
                        self.__wDown(message.message[1], message.nick)
                else:
                    pass
            else:
                '''create a new user, and return the results'''
                self.__newUser(message.nick)
                self.__downVote(message)

    def __mkKTable(self):
        '''This method is used to make a new table'''
        self.curs.execute(' CREATE TABLE IF NOT EXISTS vote (uname TEXT PRIMARY KEY, up_vote INTEGER, down_vote INTEGER, voted INTEGER, lvt TEXT)')
        self.conn.commit()

    def __mkWTable(self):
        '''This method is used to make a new table'''
        self.curs.execute(' CREATE TABLE IF NOT EXISTS vote2 (word TEXT PRIMARY KEY, up_vote INTEGER, down_vote INTEGER)')
        self.conn.commit()

    def __wordCheck(self, message):
        '''This method is used to see if a word is actually in the db already'''
        self.curs.execute('SELECT * FROM vote2 WHERE word = ?',
                          [message.message[1]])
        if len(self.curs.fetchall()) == 1 :
            '''word in db'''
            return True
        else :
            '''add word to db'''
            self.__newWord(message.message[1])
            return True

    def __nickCheck(self, message):
        '''This method will check the nick to see if they are actually a 
        person in the room. If they are, then it will see if they are in 
        the DB yet. If they are in the DB, return true, if not, return false
        '''
        nicks = self.SERVER.getUsers(message.chan)
        if message.message[1] in nicks:
            '''Ok its a user, are they in the DB?'''
            self.curs.execute('SELECT * FROM vote WHERE uname = ?',
                              [message.message[1]])
            if len(self.curs.fetchall()) == 1 :
                '''You found an exisiting user'''
                return True
            else:
                '''User is not in DB'''
                self.__newUser(message.message[1])
                return True
        else:
            '''User is not in room'''
            return False

    def __newUser(self, nick):
        '''Create a new user and add him to the db'''
        t = (nick, 0, 0, 0,
             datetime.datetime(1111, 1, 1, 0, 0, 0, 1).isoformat(' '))
        self.curs.execute('INSERT INTO vote values (?, ?, ?, ?, ?)', t)
        self.conn.commit()

    def __newWord(self, word):
        '''Create a new word and add it to the db'''
        t = (word, 0, 0)
        self.curs.execute('INSERT INTO vote2 values (?, ?, ?)', t)
        self.conn.commit()

    def __pOne(self, nick, voter):
        '''Add one up vote to specified user'''
        t = [datetime.datetime.now().isoformat(' '), voter ]
        self.curs.execute('UPDATE vote SET lvt = ?, voted = (voted + 1) WHERE uname = ?', t)
        self.curs.execute('UPDATE vote SET up_vote = (up_vote + 1) WHERE uname = ?', [nick])
        self.conn.commit()

    def __wUp(self, word, voter):
        '''Vote up a word'''
        t = [datetime.datetime.now().isoformat(' '), voter ]
        self.curs.execute('UPDATE vote SET lvt = ?, voted = (voted + 1) WHERE uname = ?', t)
        self.curs.execute('UPDATE vote2 SET up_vote = (up_vote + 1) WHERE word = ?', [word])
        self.conn.commit()

    def __wDown(self, word, voter):
        '''Vote Down a word'''
        t = [datetime.datetime.now().isoformat(' '), voter ]
        self.curs.execute('UPDATE vote SET lvt = ?, voted = (voted + 1) WHERE uname = ?', t)
        self.curs.execute('UPDATE vote2 SET down_vote = (down_vote + 1) WHERE word = ?', [word])
        self.conn.commit()

    def __mOne(self, nick, voter):
        '''Add one down vote to specified user'''
        t = [datetime.datetime.now().isoformat(' '), voter ]
        self.curs.execute('UPDATE vote SET lvt = ?, voted = (voted + 1) \
            WHERE uname = ?', t)
        self.curs.execute('UPDATE vote SET down_vote = (down_vote + 1) \
            WHERE uname = ?', [nick])
        self.conn.commit()

    def __totalKKarma(self, nick):
        '''Compute the total Karma for a user'''
        self.curs.execute('SELECT up_vote, down_vote, voted FROM vote \
            WHERE uname = ?', [nick])
        user = self.curs.fetchone()
        if user != None:
            return ([user[0] - user[1], user[0], user[1], user[2] ])
        else:
            pass

    def __totalWKarma(self, word):
        '''Compute the total Karma for a word'''
        self.curs.execute('SELECT up_vote, down_vote FROM vote2 WHERE word = ?',
                          [word])
        word = self.curs.fetchone()
        if word != None:
            return ([word[0] - word[1], word[0], word[1] ])
        else:
            pass


    def __isAllowed(self, message):
        '''This method Is used to determin if the person CASTING THE VOTE is 
        allowed to do so.
        Currently there are two constraints:
        rate limiting (4 votes a min)
        and name limiting (can't up vote yourself)
        '''
        if message.message[0] == ':++' or message.message[0] == ':<3':
            '''Upvote'''
            if message.message[1] == message.nick:
                return False
            elif self.__timeCheck(message.nick) == False:
                return False
            elif message.chan == self.SERVER.NICK:
                '''This detects a PM vote'''
                self.SERVER.sendPRIVMessage(message.nick + ' :' +
                                            'Sorry but karma is meant to be public...')
                return False
            elif self.ACL.getPermLvl(message) == 9:
                self.SERVER.sendPRIVMessage(message.nick + ' :' +
                                            'Sorry but you are on the naughty list...')
                return False
            else:
                return True
        elif message.message[0] == ':--' or message.message[0] == ':</3':
            '''Downvote'''
            if self.__timeCheck(message.nick) == False:
                return False
            elif message.chan == self.SERVER.NICK:
                self.SERVER.sendPRIVMessage(message.nick + ' :' +
                                            'Sorry but karma is meant to be public...')
                return False
            elif self.ACL.getPermLvl(message) == 9:
                self.SERVER.sendPRIVMessage(message.nick + ' :' +
                                            'Sorry but you are on the naughty list...')
                return False
            else:
                return True
        else:
            return False


    def __timeCheck(self, nick):
        '''This method is for rate limiting. If a user trys to up/down 
        vote too many times in a minute this method will return false
        2011-06-21 16:24:46.645714
        
        '''
        self.curs.execute('SELECT lvt FROM vote where uname = ?', [nick])
        aTime = self.curs.fetchone()[0]
        lvt = datetime.datetime.strptime(aTime, '%Y-%m-%d %H:%M:%S.%f')
        '''LVT is the time of the last vote by a user from the DB
        This value needs to be compared against the current time.
        If more than 15 seconds passed, make it true
        '''
        now = datetime.datetime.now()
        if (now - lvt).seconds >= 15:
            return True
        else:
            self.SERVER.sendPRIVMessage(nick + ' :' +
                                        'You voted too recently...')
            return False

    def __resetUser(self, message):
        '''Reset the karma score of a given user back to 0's'''
        if len(message.message) > 1:
            self.curs.execute('SELECT * FROM vote WHERE uname =?',
                              [message.message[1]])
        if len(self.curs.fetchall()) == 1 :
            self.curs.execute('DELETE FROM vote WHERE uname = ?',
                              [message.message[1]])
            self.__newUser(message.message[1])
        else:
            pass

    def __delWord(self, message):
        '''Delete a word from the db'''
        if len(message.message) > 1:
            self.curs.execute('DELETE FROM vote2 WHERE word = ?',
                              [message.message[1]])


    def help(self, message):
        if len(message.message) > 1 and message.message[1] == 'karma':
            cmds = list(self.ownerCommands.keys())
            cmds.extend(list(self.adminCommands.keys()))
            cmds.extend(list(self.anonCommands.keys()))
            #cmds = [x[0] for x in cmds]
            helpstr = ['Karma Help Commands: ' + str(cmds).replace(':', ''),
                       '  ,nuke_karma - ' + self.__nukeKarma.__doc__,
                       '  ,nuke_words - ' + self.__nukeWords.__doc__,
                       '  ,reset      - ' + self.__resetUser.__doc__,
                       '  ,del_word   - ' + self.__delWord.__doc__,
                       '  ++ or <3   - ' + self.__upVote.__doc__,
                       '  -- or </3  - ' + self.__downVote.__doc__,
                       '  ,karma      - ' + self.__getKarma.__doc__,
                       '  ,get_words  - ' + self.__getWords.__doc__,
                       '  ,get_users  - ' + self.__getUsers.__doc__ ]

            for hlp in helpstr:
                self.SERVER.sendPRIVMessage(message.nick + ' :' + hlp)
        else:
            pass
