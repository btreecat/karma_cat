'''
Created on Jul 14, 2011

@author: stanner
'''
from Plugin import Plugin
import socket

class ComboBreaker(Plugin):
    '''
    classdocs
    '''


    def __init__(self, acl, server):
        '''
        Constructor
        '''
        super(ComboBreaker, self).__init__(acl, server)

        self.knownCommands = {':,help': self.help}

        self.prevMsg = None

        self.heatingUp = 0

    def read(self, message):

        try:
            self.knownCommands[message.message[0]](message)
            #print("commands")
        except KeyError:
            pass

        if message.message == self.prevMsg:
            '''This means that the previous message is the same as the current one'''
            self.heatingUp = self.heatingUp + 1
            #print(self.heatingUp)
            self.prevMsg = message.message
        else:
            if self.heatingUp > 1:
                cmbo = ""
                for i in range(self.heatingUp):
                    cmbo = cmbo + "C-"
                cmbo = cmbo + "COMBO BREAKER!"
                self.SERVER.sendPRIVMessage(message.chan + " :" + cmbo)
                self.heatingUp = 0
                self.prevMsg = message.message
            else:
                self.prevMsg = message.message



    def help(self, message):
        if len(message.message) > 1 and message.message[1] == 'cccb':
            #print("Shit Shit")
            helpstr = ['ComboBreaker Help Commands: ',
                       '  none - When the same message is sent 3 times or more, a combo is created.' ]

            for hlp in helpstr:
                self.SERVER.sendPRIVMessage(message.nick + ' :' + hlp)

        if len(message.message) > 1 and message.message[1] == 'cccp':
            #print("Shit Shit")
            helpstr = ['CCCP Help Commands: ',
                       '  none - nuke now inbound towards ' + self.__getIP(message.host)]

            for hlp in helpstr:
                self.SERVER.sendPRIVMessage(message.nick + ' :' + hlp)


    def __getIP(self, host):
        try:
            return socket.gethostbyname(host)
        except:
            return host
