'''
This is Karma Cat! This is my attempt at creating a simple IRC Bot
to implement a Karma system!

This is based off code from :
http://oreilly.com/pub/h/1968
http://www.osix.net/modules/article/?id=780

Version| 1.0 Alpha-3
Author| btreecat
URL: https://code.google.com/p/karma-cat/
REPO: hg clone https://karma-cat.googlecode.com/hg/ karma-cat
LICENSE: GPL3
'''

'''Imports'''
#import sys
#import socket
from Parser import Parser
from Server import Server
from ACL import ACL

'''Pulgins'''
from Admin import Admin
from Karma import Karma
from URL import URL
from cccb import ComboBreaker
#import os



class karma_kat:

    def __init__(self):

        '''Basic Configuration Section begins here'''
        #This Dictionary is for the server config
        self.configDic = {'HOST': 'chat.freenode.net',
                          'PORT': 6667,
                          'NICK': 'Dr_Meowzers_b',
                          'IDENT': 'karma_kat',
                          'REALNAME': 'Karma Kat',
                          'CHANNELINIT': '#btreecat' }

        #Pets Owner
        self.OWNER = 'btreecat'

        #self.ADMINS = [self.OWNER, 'stanner']

        self.ACL = ACL(self.OWNER)

        self.SERVER = Server(self.configDic)

        #create the plugins
        self.admin = Admin(self.ACL, self.SERVER)
        self.karma = Karma(self.ACL, self.SERVER)
        self.url = URL(self.ACL, self.SERVER)
        #self.cccb = ComboBreaker(self.ACL, self.SERVER)
        self.plugins = [self.admin, self.karma, self.url]


        self.SERVER.connectToServer()
        self.parser = Parser(self.SERVER)
        self.__registerPlugins()
        self.parser.parseEngine()


    def __registerPlugins(self):

        for plugin in self.plugins:
            plugin.register(self.parser)



'''
Now we actually need to run our class
'''
kat = karma_kat()

